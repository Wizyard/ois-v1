import java.util.Scanner;

public class Gravitacija {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        double m = 5.972 * (Math.pow(10, 24)); // masa zemlje
        double r = 6.371 * (Math.pow(10, 6)); // polmer zemlje
        double c = 6.674 * (Math.pow(10, -11)); //gravitacijska konstanta
        
        double nadmorskaVisina = sc.nextDouble();
        
        double pospesek = (c * m)/Math.pow((r + nadmorskaVisina), 2);
        
        System.out.println();
        izpis(nadmorskaVisina, pospesek);
        System.out.println();
        System.out.println("OIS je zakon!");
    }
    
    static void izpis(double nmVisina, double gPospesek) {      
        System.out.println("Nadmorska visina: " + nmVisina);    
        System.out.println("Gravitacijski pospesek: " + gPospesek);
    }
}